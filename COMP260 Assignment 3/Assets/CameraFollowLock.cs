﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowLock : MonoBehaviour {

    public Transform target;
    public Vector3 zOffset = new Vector3(0, 0, -10);

    void Update()
    {
        transform.position = target.position + zOffset;
    }

}
