﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {

	    // Use this for initialization
	    public float maxSpeed = 5.0f;
	    public Rigidbody bodyRigid;
	    public Rigidbody velocity;



	    public Transform startingPos;
	    private Rigidbody rigidbody;

	    void Start () {
		        bodyRigid = GetComponent<Rigidbody>();
		        rigidbody = GetComponent<Rigidbody>();
		        ResetPosition();
		    }

	    public void ResetPosition() {
		        // teleport to the starting position
		        rigidbody.MovePosition(startingPos.position);
		        rigidbody.velocity = Vector3.zero;
		    }


	    void Update()
	    {
		        // get the input values
		        Vector2 direction;
		        direction.x = Input.GetAxis("Horizontal");
		        direction.y = Input.GetAxis("Vertical");

		        bodyRigid.velocity = maxSpeed * direction * Time.deltaTime * 80f;
		    }
}

