﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(GameObject))]
public class SwitchController : MonoBehaviour {

    public GameObject Switch;
    public GameObject MazeRed; //1
    public GameObject MazeGreen; //2 
    public GameObject MazeBlue; //3
    public GameObject Player;
    public Material SwitchType;

    public void Start()
    {
        /*MazeRed = GameObject.Find("Maze Red/"); //1
        MazeGreen = GameObject.Find("Maze Green/"); //2 
        MazeBlue = GameObject.Find("Maze Blue/"); //3
        Player = GameObject.Find("Player");*/
        MazeRed.gameObject.SetActive(false);
        MazeGreen.gameObject.SetActive(true);
        MazeBlue.gameObject.SetActive(false);
    }

    int currentMaze = 2;

    private void OnTriggerEnter(Collider collider)
    {
        Debug.Log("collision");
        if (SwitchType.name == "R-G Switch")
        {
            Debug.Log("R-G Switch");
            switch (currentMaze)
            {
                case 2:
                    MazeRed.gameObject.SetActive(true);
                    MazeGreen.gameObject.SetActive(false);
                    currentMaze = 1;
                    Debug.Log("current maze " + currentMaze);
                    break;
                case 1:
                    MazeGreen.gameObject.SetActive(true);
                    MazeRed.gameObject.SetActive(false);
                    currentMaze = 2;
                    Debug.Log("current maze " + currentMaze);
                    break;
            }
        }

        if (SwitchType.name == "G-B Switch")
        {
            switch (currentMaze)
            {
                case 2:
                    MazeBlue.gameObject.SetActive(true);
                    MazeGreen.gameObject.SetActive(false);
                    currentMaze = 2;
                    break;
                case 3:
                    MazeGreen.gameObject.SetActive(true);
                    MazeBlue.gameObject.SetActive(false);
                    currentMaze = 2;
                    break;
            }
        }

        if (SwitchType.name == "R-G Switch")
        {
            switch (currentMaze)
            {
                case 2:
                    MazeRed.gameObject.SetActive(true);
                    MazeGreen.gameObject.SetActive(false);
                    currentMaze = 1;
                    break;
                case 1:
                    MazeGreen.gameObject.SetActive(true);
                    MazeRed.gameObject.SetActive(false);
                    currentMaze = 2;
                    break;
            }
        }
    }


    public void Update()
    {

    }
} 